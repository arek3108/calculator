package project.application;

import org.junit.Test;
import project.calculator.Units;

import static org.junit.Assert.*;

/**
 * Created by arek on 30.10.18.
 */
public class ModeTest {

    @Test
    public void testGetMode() throws Exception {
        assertEquals(Mode.WEB.getMode(), "web");
        assertEquals(Mode.CONSOLE.getMode(), "console");
        assertEquals(Mode.ARGS.getMode(), "args");
    }
}