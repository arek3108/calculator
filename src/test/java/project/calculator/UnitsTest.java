package project.calculator;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by arek on 30.10.18.
 */
public class UnitsTest {

    @Test
    public void testGetId() throws Exception {
        assertEquals(Units.Meters.getId(), 1);
        assertEquals(Units.Feets.getId(), 2);
        assertEquals(Units.Nautical_Miles.getId(), 3);
    }

    @Test
    public void testGetConversionToMeters() throws Exception {
        assertEquals(Units.Meters.getConversionToMeters(), BigDecimal.valueOf(1));
    }

    @Test
    public void testGetById() throws Exception {
        assertEquals(Units.getByName("Meters"), Units.Meters);
        assertNull(Units.getByName(""));
    }

    @Test
    public void testGetByName() throws Exception {
        assertEquals(Units.getById(1), Units.Meters);
        assertNull(Units.getById(-1));
    }
}