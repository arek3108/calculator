package project.calculator;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by arek on 30.10.18.
 */
public class ResultTest {

    @Test
    public void testGetResult() throws Exception {
        Result result = new Result(BigDecimal.ONE);
        assertEquals(result.getResult(), BigDecimal.ONE);
    }
}