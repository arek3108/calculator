package project.calculator;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by arek on 30.10.18.
 */
public class CalculateRequestTest {

    @Test
    public void testGetAndSet() throws Exception {
        CalculateRequest calculateRequest = new CalculateRequest();
        calculateRequest.setFirstNumber(BigDecimal.ONE);
        calculateRequest.setSecondNumber(BigDecimal.ONE);
        calculateRequest.setInputUnits(Units.Meters);
        calculateRequest.setResultUnits(Units.Meters);
        calculateRequest.setOperations(Operations.Add);
        assertEquals(calculateRequest.getFirstNumber(), BigDecimal.ONE);
        assertEquals(calculateRequest.getSecondNumber(), BigDecimal.ONE);
        assertEquals(calculateRequest.getInputUnits(), Units.Meters);
        assertEquals(calculateRequest.getResultUnits(), Units.Meters);
        assertEquals(calculateRequest.getOperations(), Operations.Add);
    }
}