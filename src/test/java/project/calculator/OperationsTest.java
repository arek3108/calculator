package project.calculator;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by arek on 30.10.18.
 */
public class OperationsTest {

    @Test
    public void testApply() throws Exception {
        assertEquals(Operations.Add.apply(BigDecimal.ONE, BigDecimal.ONE), BigDecimal.valueOf(2).setScale(10));
        assertEquals(Operations.Substract.apply(BigDecimal.ONE, BigDecimal.ONE), BigDecimal.ZERO.setScale(10));
        assertEquals(Operations.Multiply.apply(BigDecimal.ONE, BigDecimal.ONE), BigDecimal.ONE.setScale(10));
        assertEquals(Operations.Divide.apply(BigDecimal.ONE, BigDecimal.ONE), BigDecimal.ONE.setScale(10));
    }

    @Test
    public void testGetId() throws Exception {
        assertEquals(Operations.Add.getId(), 1);
        assertEquals(Operations.Substract.getId(), 2);
        assertEquals(Operations.Multiply.getId(), 3);
        assertEquals(Operations.Divide.getId(), 4);
    }

    @Test
    public void testGetById() throws Exception {
        assertEquals(Operations.getByName("Add"), Operations.Add);
        assertNull(Operations.getByName(""));
    }

    @Test
    public void testGetByName() throws Exception {
        assertEquals(Operations.getById(1), Operations.Add);
        assertNull(Operations.getById(-1));
    }
}