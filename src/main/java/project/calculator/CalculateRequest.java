package project.calculator;

import java.math.BigDecimal;

public class CalculateRequest {
    private Units inputUnits;
    private Units resultUnits;
    private Operations operations;
    private BigDecimal firstNumber;
    private BigDecimal secondNumber;

    public Units getInputUnits() {
        return inputUnits;
    }

    public void setInputUnits(Units inputUnits) {
        this.inputUnits = inputUnits;
    }

    public Units getResultUnits() {
        return resultUnits;
    }

    public void setResultUnits(Units resultUnits) {
        this.resultUnits = resultUnits;
    }

    public Operations getOperations() {
        return operations;
    }

    public void setOperations(Operations operations) {
        this.operations = operations;
    }

    public BigDecimal getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(BigDecimal firstNumber) {
        this.firstNumber = firstNumber;
    }

    public BigDecimal getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(BigDecimal secondNumber) {
        this.secondNumber = secondNumber;
    }
}
