package project.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by arek on 27.10.18.
 */
public enum Operations {
    Add(1) {
        BigDecimal apply(BigDecimal firstNumber, BigDecimal secondNumber) {
            return firstNumber.setScale(10).add(secondNumber);
        }
    },
    Substract(2) {
        BigDecimal apply(BigDecimal firstNumber, BigDecimal secondNumber) {
            return firstNumber.setScale(10).subtract(secondNumber);
        }
    },
    Multiply(3) {
        BigDecimal apply(BigDecimal firstNumber, BigDecimal secondNumber) {
            return firstNumber.setScale(10).multiply(secondNumber);
        }
    },
    Divide(4) {
        BigDecimal apply(BigDecimal firstNumber, BigDecimal secondNumber) {
            return firstNumber.setScale(10).divide(secondNumber, RoundingMode.HALF_UP);
        }
    };

    private int id;

    Operations(int id) {
        this.id = id;
    }

    abstract BigDecimal apply(BigDecimal firstNumber, BigDecimal secondNumber);

    public int getId() {
        return id;
    }

    public static Operations getById(int id) {
        for (Operations operation: Operations.values()) {
            if (operation.getId() == id) {
                return operation;
            }
        }
        return null;
    }

    public static Operations getByName(String name) {
        for (Operations operation: Operations.values()) {
            if (operation.name().toLowerCase().equals(name.toLowerCase())) {
                return operation;
            }
        }
        return null;
    }
}
