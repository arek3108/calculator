package project.calculator;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public BigDecimal getResult(Units inputUnit, Units resultUnit, Operations operation, BigDecimal firstNumber, BigDecimal secondNumber) {
        BigDecimal firstNumberInMeters = firstNumber.multiply(inputUnit.getConversionToMeters());
        BigDecimal secondNumberInMeters = secondNumber.multiply(inputUnit.getConversionToMeters());
        BigDecimal resultInMeters = operation.apply(firstNumberInMeters, secondNumberInMeters);
        return resultInMeters.divide(resultUnit.getConversionToMeters(), RoundingMode.HALF_UP);
    }
}
