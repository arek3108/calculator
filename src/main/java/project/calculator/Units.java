package project.calculator;

import java.math.BigDecimal;

/**
 * Created by arek on 27.10.18.
 */
public enum Units {
    Meters(1, BigDecimal.valueOf(1)),
    Feets(2, BigDecimal.valueOf(0.3048)),
    Nautical_Miles(3, BigDecimal.valueOf(1852));

    private int id;
    private BigDecimal conversionToMeters;

    Units(int id, BigDecimal conversionToMeters) {
        this.id = id;
        this.conversionToMeters = conversionToMeters;
    }

    public int getId() {
        return id;
    }

    public BigDecimal getConversionToMeters() {
        return conversionToMeters;
    }

    public static Units getById(int id) {
        for (Units unit: Units.values()) {
            if (unit.getId() == id) {
                return unit;
            }
        }
        return null;
    }

    public static Units getByName(String name) {
        for (Units unit: Units.values()) {
            if (unit.name().toLowerCase().equals(name.toLowerCase())) {
                return unit;
            }
        }
        return null;
    }
}
