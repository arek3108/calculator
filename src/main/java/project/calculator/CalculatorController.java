package project.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CalculatorController {

    private final CalculatorService calculatorService;

    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @RequestMapping(value = "/calculator/calculate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> calculate(@RequestBody CalculateRequest calculateRequest) {
        Result result = new Result(calculatorService.getResult(calculateRequest.getInputUnits(),
                                                                calculateRequest.getResultUnits(),
                                                                calculateRequest.getOperations(),
                                                                calculateRequest.getFirstNumber(),
                                                                calculateRequest.getSecondNumber()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}