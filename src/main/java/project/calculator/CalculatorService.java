package project.calculator;

import java.math.BigDecimal;

public interface CalculatorService {
    /**
     * Gets result of calculation.
     *
     * @param  inputUnit        The unit of input parameters
     * @param  resultUnit       The unit of result
     * @param  operation        The operation which should be done
     * @param  firstNumber      First number
     * @param  secondNumber     Second number
     * @return                  Result of calculation
     */
    BigDecimal getResult(Units inputUnit, Units resultUnit, Operations operation, BigDecimal firstNumber, BigDecimal secondNumber);
}
