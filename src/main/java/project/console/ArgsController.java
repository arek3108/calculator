package project.console;

import jdk.nashorn.internal.ir.annotations.Ignore;
import project.calculator.CalculatorService;
import project.calculator.CalculatorServiceImpl;
import project.calculator.Operations;
import project.calculator.Units;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by arek on 28.10.18.
 */
@Ignore
public class ArgsController {
    public void run(String... strings) {
        if (strings.length < 5) {
            System.out.println("Wrong number of arguments!\n");
            showHelp();
            return;
        }
        CalculatorService calculatorService = new CalculatorServiceImpl();
        Units inputUnits = Units.getByName(strings[0]);
        Units resultUnits = Units.getByName(strings[1]);
        Operations operation = Operations.getByName(strings[2]);
        BigDecimal firstNumber, secondNumber;
        try {
            firstNumber = new BigDecimal(strings[3].replace(",", "."));
            secondNumber = new BigDecimal(strings[4].replace(",", "."));
        } catch (NumberFormatException e) {
            System.out.println("First and second number should be a number. '" + strings[3] + "' or '" + strings[4] + "' is not a number!");
            return;
        }
        BigDecimal result = calculatorService.getResult(inputUnits, resultUnits, operation, firstNumber, secondNumber);
        System.out.println("Result: " + result.toString());
    }

    private void showHelp() {
        System.out.println("Calculator - Help:\n\nYou need to add 5 arguments in order: [inputUnits] [resultUnits] [operation] [firstNumber] [secondNumber]\n");
        System.out.println("[inputUnit] - The unit of input parameters, passible options: " + String.join(", ", Arrays.asList(Units.values()).stream().map(Enum::name).collect(Collectors.toList())));
        System.out.println("[resultUnit] - The unit of result, passible options: " + String.join(", ", Arrays.asList(Units.values()).stream().map(Enum::name).collect(Collectors.toList())));
        System.out.println("[operation] - The operation which should be done, passible options: " + String.join(", ", Arrays.asList(Operations.values()).stream().map(Enum::name).collect(Collectors.toList())));
        System.out.println("[firstNumber] - First number");
        System.out.println("[secondNumber] - Second number");
    }
}
