package project.console;

import jdk.nashorn.internal.ir.annotations.Ignore;
import project.calculator.CalculatorService;
import project.calculator.CalculatorServiceImpl;
import project.calculator.Operations;
import project.calculator.Units;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 * Created by arek on 27.10.18.
 */
@Ignore
public class ConsoleController {
    private Scanner scanner = new Scanner(System. in);

    public void run() {
        Units inputUnits, resultUnits;
        Operations operation;
        BigDecimal firstNumber, secondNumber, exit = BigDecimal.ZERO;

        CalculatorService calculatorService = new CalculatorServiceImpl();

        while (exit.compareTo(BigDecimal.ONE) != 0) {
            showInputUnitsQuestion();
            showUnits();
            inputUnits = Units.getById(readNumber().intValue());

            showFirstNumberQuestion();
            firstNumber = readNumber();

            showSecondNumberQuestion();
            secondNumber = readNumber();

            showOperationsQuestion();
            showOperations();
            operation = Operations.getById(readNumber().intValue());

            showOutputUnitsQuestion();
            showUnits();
            resultUnits = Units.getById(readNumber().intValue());

            BigDecimal result = calculatorService.getResult(inputUnits, resultUnits, operation, firstNumber, secondNumber);
            showResult(result);

            showEndingQuestion();
            exit = readNumber();
        }
    }

    private void showResult(BigDecimal result) {
        System.out.println("Result: " + result.toString());
    }

    private void showInputUnitsQuestion() {
        System.out.println("Please select ID of input parameters unit: ");
    }

    private void showFirstNumberQuestion() {
        System.out.print("Please write first number: ");
    }

    private void showEndingQuestion() {
        System.out.print("Please write 1 to exit: ");
    }

    private void showSecondNumberQuestion() {
        System.out.print("Please write second number: ");
    }

    private void showOperationsQuestion() {
        System.out.println("Please select ID of operation: ");
    }

    private void showOutputUnitsQuestion() {
        System.out.println("Please select ID of result unit: ");
    }

    private void showUnits() {
        for (Units unit: Units.values()) {
            System.out.println(unit.getId() + ": " + unit.name());
        }
        System.out.print("Write ID: ");
    }

    private void showOperations() {
        for (Operations unit: Operations.values()) {
            System.out.println(unit.getId() + ": " + unit.name());
        }
        System.out.print("Write ID: ");
    }

    private BigDecimal readNumber() {
        String input = scanner.nextLine().replace(",", ".");
        BigDecimal outNumber;
        try {
            outNumber = new BigDecimal(input).setScale(10, RoundingMode.HALF_UP);
        } catch (NumberFormatException e) {
            System.out.println("You should give a number!");
            System.out.print("Try again: ");
            outNumber = readNumber();
        }
        return outNumber;
    }
}
