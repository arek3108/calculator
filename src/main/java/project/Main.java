package project;

import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import project.application.Mode;
import project.console.ArgsController;
import project.console.ConsoleController;

@SpringBootApplication
public class Main implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication springApplication =
                new SpringApplicationBuilder()
                        .sources(Main.class)
                        .web(args.length > 0 && args[0].equals(Mode.WEB.getMode()))
                        .bannerMode(Banner.Mode.OFF)
                        .build();
        springApplication.run(args);
    }

    @Override
    public void run(String... strings) throws Exception {
        if (strings.length > 0 && !strings[0].equals(Mode.WEB.getMode())) {
            if(strings[0].equals(Mode.CONSOLE.getMode())) {
                new ConsoleController().run();
            } else {
                new ArgsController().run(strings);
            }
        }
    }
}