package project.application;

/**
 * Created by arek on 28.10.18.
 */
public enum Mode {
    WEB("web"),
    CONSOLE("console"),
    ARGS("args");

    private String mode;

    Mode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }
}
