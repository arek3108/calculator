'use strict'

var app = angular.module('app', []);
app.constant("CONSTANTS", {
	calculatorCalculateUrl : "/calculator/calculate/"
});

app.service('PageService', [ "$http", "CONSTANTS", function($http, CONSTANTS) {
	this.calculatorCalculate = function(messageBody) {
		return $http.post(CONSTANTS.calculatorCalculateUrl, messageBody);
	}
} ]);

app.controller("PageController", [ "$scope", "PageService",
	function($scope, PageService) {
		$scope.inputUnit = 'Meters';
		$scope.resultUnit = 'Meters';
		$scope.operation = 'Add';
		$scope.firstNumber = 0;
		$scope.secondNumber = 0;
		$scope.result = '';

		$scope.calculatorCalculate = function() {
			var messageBody = {
				"inputUnits": $scope.inputUnit,
				"resultUnits": $scope.resultUnit,
				"operations": $scope.operation,
				"firstNumber": $scope.firstNumber ? $scope.firstNumber.replace(",", ".") : 0,
				"secondNumber": $scope.secondNumber ? $scope.secondNumber.replace(",", ".") : 0

			}
			PageService.calculatorCalculate(messageBody).then(
				function(data) { $scope.result = data.data.result; },
				function(data) {
					$scope.result = data.message;
					if (data.status == 400) {
						$scope.result = "Bad Request. Probably wrong data.";
					}
				}
			);
		};
	} ]);